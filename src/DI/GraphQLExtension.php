<?php
declare(strict_types = 1);

namespace FS\GraphQL\DI;

use FS\GraphQL\DI\Registrators\EnumsRegistrator;
use FS\GraphQL\DI\Registrators\InputTypesRegistrator;
use FS\GraphQL\DI\Registrators\MutationsRegistrator;
use FS\GraphQL\DI\Registrators\OutputTypesRegistrator;
use FS\GraphQL\DI\Registrators\QueriesRegistrator;
use FS\GraphQL\Exception\UnknownTypeDefinitionException;
use FS\GraphQL\SchemaFactory;
use FS\GraphQL\Types\DateTimeType;
use Nette\DI\CompilerExtension;
use Nette\DI\ServiceDefinition;
use Nette\DI\Statement;

class GraphQLExtension extends CompilerExtension
{

	private $defaults = [
		'scalars' => [
			'DateTime' => DateTimeType::class,
		],
		'enums' => [],
		'outputTypes' => [],
		'inputTypes' => [],
		'queries' => [],
		'mutations' => [],
	];

	private $scalars = [
		'Int' => 'Int',
		'Float' => 'float',
		'String' => 'string',
		'Boolean' => 'boolean',
		'ID' => 'id',
	];

	public function loadConfiguration(): void
	{
		$builder = $this->getContainerBuilder();
		$config = $this->validateConfig($this->defaults);

		foreach ($config['scalars'] as $scalarName => $scalarClass) {
			$builder->addDefinition($this->prefix('scalar.' . $scalarName))->setClass($scalarClass);
		}

		(new EnumsRegistrator)->register($this, $config['enums']);
		(new OutputTypesRegistrator)->register($this, $config['outputTypes'], $config['enums']);
		(new InputTypesRegistrator)->register($this, $config['inputTypes'], $config['enums']);
		(new QueriesRegistrator)->register($this, $config['queries']);
		(new MutationsRegistrator)->register($this, $config['mutations']);

		$builder
			->addDefinition($this->prefix('schemaFactory'))
			->setClass(SchemaFactory::class)
			->addSetup('?->addQueryType(?)', ['@self', $builder->getDefinition($this->prefix('query'))])
			->addSetup('?->addMutationType(?)', ['@self', $builder->getDefinition($this->prefix('mutation'))]);
	}

	/**
	 * @param $type
	 * @return mixed|ServiceDefinition|Statement
	 */
	public function resolveGraphQLType($type)
	{
		$listOf = FALSE;
		if (is_array($type)) {
			$listOf = TRUE;
			$type = $type[0];
		}

		$nonNull = FALSE;
		$pattern = '~(.+)!$~';
		if (preg_match($pattern, $type)) {
			$nonNull = TRUE;
			$type = preg_replace($pattern, '$1', $type);
		}

		if (array_key_exists($type, $this->scalars)) {
			$type = new Statement('\GraphQL\Type\Definition\Type::?()', [$this->scalars[$type]]);
		} else {
			$type = $this->getTypeDefinition($type);
		}

		if ($nonNull) {
			$type = new Statement('\GraphQL\Type\Definition\Type::nonNull(?)', [$type]);
		}
		if ($listOf) {
			return new Statement('\GraphQL\Type\Definition\Type::listOf(?)', [$type]);
		}
		return $type;
	}

	private function getTypeDefinition(string $name): ServiceDefinition
	{
		$builder = $this->getContainerBuilder();
		$definition = NULL;
		$allowedNamespaces = ['scalar', 'inputType', 'outputType', 'enum'];
		foreach ($allowedNamespaces as $namespace) {
			if ($builder->hasDefinition($this->prefix("$namespace.$name"))) {
				$definition = $builder->getDefinition($this->prefix("$namespace.$name"));
				break;
			}
		}

		if ($definition === NULL) {
			throw new UnknownTypeDefinitionException(
				"Cannot find definition for type '$name'. Did you register it in configuration file?"
			);
		}
		return $definition;
	}

}
