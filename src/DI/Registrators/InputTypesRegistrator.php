<?php
declare(strict_types = 1);

namespace FS\GraphQL\DI\Registrators;

use FS\GraphQL\DI\GraphQLExtension;
use GraphQL\Type\Definition\InputObjectType;
use Nette\DI\Statement;

class InputTypesRegistrator
{
	/**
	 * @param GraphQLExtension $extension
	 * @param array $inputTypes
	 * @param array $allEnumValues
	 */
	public function register(GraphQLExtension $extension, array $inputTypes, array $allEnumValues)
	{
		$builder = $extension->getContainerBuilder();

		foreach ($inputTypes as $typeName => $typeDetails) {
			$builder
				->addDefinition($extension->prefix("inputType.$typeName"))
				->setClass(InputObjectType::class);
		}

		foreach ($inputTypes as $typeName => $inputFields) {
			$builder
				->getDefinition($extension->prefix("inputType.$typeName"))
				->setArguments([
					'config' => [
						'name' => $typeName,
						'fields' => $this->buildInputFields($extension, $inputFields, $allEnumValues),
					],
				]);
		}
	}

	/**
	 * @param GraphQLExtension $extension
	 * @param array $inputFields
	 * @param array $allEnumValues
	 * @return array
	 */
	private function buildInputFields(GraphQLExtension $extension, array $inputFields, array $allEnumValues)
	{
		$output = [];
		foreach ($inputFields as $fieldName => $fieldType) {
			$defaultValue = NULL;
			if ($fieldType instanceof Statement) {
				$default = $fieldType->arguments['default'];
				$fieldType = $fieldType->getEntity();
				if ($extension->resolveGraphQLType($fieldType) instanceof Statement) {
					$defaultValue = $default;
				} else {
					$defaultValue = $allEnumValues[$fieldType][$default];
				}
			}
			$output[$fieldName] = [
				'type' => $extension->resolveGraphQLType($fieldType),
				'defaultValue' => $defaultValue,
			];
		}
		return $output;
	}
}
