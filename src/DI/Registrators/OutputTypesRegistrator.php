<?php
declare(strict_types = 1);

namespace FS\GraphQL\DI\Registrators;

use FS\GraphQL\DI\GraphQLExtension;
use FS\GraphQL\Exception\OutputFieldNotCallableException;
use FS\GraphQL\Exception\ResolverNotDefinedException;
use Nette\DI\Statement;

class OutputTypesRegistrator
{

	/**
	 * @param GraphQLExtension $extension
	 * @param array $outputTypes
	 * @param array $allEnumValues
	 * @throws ResolverNotDefinedException
	 */
	public function register(GraphQLExtension $extension, array $outputTypes, array $allEnumValues)
	{
		$builder = $extension->getContainerBuilder();

		foreach ($outputTypes as $typeName => $typeDetails) {
			$builder
				->addDefinition($extension->prefix("outputType.$typeName"))
				->setClass(\GraphQL\Type\Definition\ObjectType::class);
		}

		foreach ($outputTypes as $typeName => $typeDetails) {
			if (!isset($typeDetails['resolver'])) {
				throw new ResolverNotDefinedException(
					"You must define 'resolver' in '{$extension->prefix('outputType')}.$typeName'."
				);
			}

			$resolverDefinition = $builder->addDefinition($extension->prefix('outputTypeResolver.' . $typeName));
			if($typeDetails['resolver'] instanceof Statement) {
				$class = $typeDetails['resolver']->getEntity();
				$args = \Nette\DI\Helpers::expand($typeDetails['resolver']->arguments, $builder->parameters);
				$resolverDefinition->setClass($class, $args);
			} else {
				$resolverDefinition->setClass($typeDetails['resolver']);
			}

			$builder
				->getDefinition($extension->prefix("outputType.$typeName"))
				->setArguments([
					'config' => [
						'name' => $typeName,
						'fields' => $this->buildOutputFields(
							$extension,
							$typeDetails['fields'],
							$resolverDefinition,
							$allEnumValues
						),
						'description' => $typeDetails['description'] ?? null,
					],
				]);
		}
	}

	/**
	 * @param GraphQLExtension $extension
	 * @param array $fields
	 * @param \Nette\DI\ServiceDefinition $resolverDefinition
	 * @param array $allEnumValues
	 * @return array
	 * @throws OutputFieldNotCallableException
	 */
	private function buildOutputFields(GraphQLExtension $extension, array $fields, \Nette\DI\ServiceDefinition $resolverDefinition, array $allEnumValues)
	{
		$output = [];
		foreach ($fields as $fieldName => $fieldDetails) {

			if (!is_callable([$resolverDefinition->class, $fieldName])) {
				throw new OutputFieldNotCallableException(
					"You must implement method '$fieldName' in class '$resolverDefinition->class'."
				);
			}

			if (is_array($fieldDetails) && isset($fieldDetails['next'], $fieldDetails['arguments'])) { // output type with 'arguments' and 'next'
				$type = $fieldDetails['next'];
			} else {
				$type = $fieldDetails;
			}

			$output[$fieldName] = [
				'type' => $extension->resolveGraphQLType($type),
				'resolve' => [$resolverDefinition, $fieldName],
			];
			if (isset($fieldDetails['arguments'])) {
				$output[$fieldName]['args'] = $this->buildArguments($extension, $fieldDetails['arguments'], $allEnumValues);
			}
		}
		return $output;
	}

	/**
	 * @param GraphQLExtension $extension
	 * @param array $arguments
	 * @param array $allEnumValues
	 * @return array
	 */
	private function buildArguments(GraphQLExtension $extension, array $arguments, array $allEnumValues)
	{
		$output = [];
		foreach ($arguments as $argumentName => $argumentDetails) {

			$defaultValue = NULL;
			if ($argumentDetails instanceof Statement) {
				$default = $argumentDetails->arguments['default'];
				$argumentDetails = $argumentDetails->getEntity();
				if ($extension->resolveGraphQLType($argumentDetails) instanceof Statement) {
					$defaultValue = $default;
				} else {
					$defaultValue = $allEnumValues[$argumentDetails][$default];
				}
			}

			$output[$argumentName] = [
				'type' => $extension->resolveGraphQLType($argumentDetails),
				'defaultValue' => $defaultValue,
			];
		}
		return $output;
	}

}
