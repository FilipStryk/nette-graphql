<?php
declare(strict_types = 1);

namespace FS\GraphQL\DI\Registrators;

use FS\GraphQL\DI\GraphQLExtension;
use FS\GraphQL\Exception\OutputFieldNotCallableException;
use FS\GraphQL\Exception\ResolverNotDefinedException;
use FS\GraphQL\IResolver;
use GraphQL\Type\Definition\ObjectType;

class QueriesRegistrator
{

	public function register(GraphQLExtension $extension, array $queries)
	{
		$builder = $extension->getContainerBuilder();

		$fields = [];
		foreach ($queries as $queryName => $queryDetails) {

			if (!isset($queryDetails['resolver'])) {
				throw new ResolverNotDefinedException(
					"You must define 'resolver' in '{$extension->prefix('queries')}.$queryName'."
				);
			}

			if (!class_exists($queryDetails['resolver'])) {
				throw new ResolverNotDefinedException(
					"Resolver class defined in '{$extension->prefix('queries')}.$queryName' does not exist."
				);
			}

			if (!(new \ReflectionClass($queryDetails['resolver']))->hasMethod('__invoke')) {
				throw new OutputFieldNotCallableException(
					"Resolver in '{$extension->prefix('queries')}.$queryName' must implement __invoke method."
				);
			}

			if (!isset($queryDetails['next'])) {
				throw new \Exception("You must define next intermediate level in graph using 'next' key.");
			}

			$fields[$queryName] = [
				'type' => $extension->resolveGraphQLType($queryDetails['next']),
				'resolve' => $builder->addDefinition($extension->prefix('queryResolver.' . $queryName))->setClass($queryDetails['resolver']),
				'description' => isset($queryDetails['description']) ? $queryDetails['description'] : null,
			];
			if (isset($queryDetails['arguments'])) {
				$fields[$queryName]['args'] = $this->buildArguments($extension, $queryDetails['arguments']);
			}
		}

		$builder
			->addDefinition($extension->prefix('query'))
			->setClass(ObjectType::class)
			->setArguments([
				'config' => [
					'name' => 'Query',
					'fields' => $fields,
				],
			]);
	}

	private function buildArguments(GraphQLExtension $extension, array $arguments)
	{
		$output = [];
		foreach ($arguments as $argumentName => $argumentDetails) {
			$output[$argumentName] = [
				'type' => $extension->resolveGraphQLType($argumentDetails),
			];
		}
		return $output;
	}

}
