<?php
declare(strict_types = 1);

namespace FS\GraphQL\DI\Registrators;

use FS\GraphQL\DI\GraphQLExtension;
use FS\GraphQL\Exception\OutputFieldNotCallableException;
use FS\GraphQL\Exception\ResolverNotDefinedException;
use FS\GraphQL\IResolver;
use GraphQL\Type\Definition\ObjectType;

class MutationsRegistrator
{

	public function register(GraphQLExtension $extension, array $mutations)
	{
		$builder = $extension->getContainerBuilder();

		$fields = [];
		foreach ($mutations as $mutationName => $mutationDetails) {

			if (!isset($mutationDetails['resolver'])) {
				throw new ResolverNotDefinedException(
					"You must define 'resolver' in '{$extension->prefix('mutations')}.$mutationName'."
				);
			}

			if (!class_exists($mutationDetails['resolver'])) {
				throw new ResolverNotDefinedException(
					"Resolver class defined in '{$extension->prefix('mutations')}.$mutationName' does not exist."
				);
			}

			if (!(new \ReflectionClass($mutationDetails['resolver']))->hasMethod('__invoke')) {
				throw new OutputFieldNotCallableException(
					"Resolver in '{$extension->prefix('mutations')}.$mutationName' must implement __invoke method"
				);
			}

			if (!isset($mutationDetails['next'])) {
				throw new \Exception("You must define next intermediate level in graph using 'next' key.");
			}

			$fields[$mutationName] = [
				'type' => $extension->resolveGraphQLType($mutationDetails['next']),
				'resolve' => $builder->addDefinition($extension->prefix($mutationName))->setClass($mutationDetails['resolver']),
				'description' => isset($mutationDetails['description']) ? $mutationDetails['description'] : null,
			];
			if (isset($mutationDetails['arguments'])) {
				$fields[$mutationName]['args'] = $this->buildArguments($extension, $mutationDetails['arguments']);
			}
		}

		$builder
			->addDefinition($extension->prefix('mutation'))
			->setClass(ObjectType::class)
			->setArguments([
				'config' => [
					'name' => 'Mutation',
					'fields' => $fields,
				],
			]);
	}

	private function buildArguments(GraphQLExtension $extension, array $arguments)
	{
		$output = [];
		foreach ($arguments as $argumentName => $argumentDetails) {
			$output[$argumentName] = [
				'type' => $extension->resolveGraphQLType($argumentDetails),
			];
		}
		return $output;
	}

}
