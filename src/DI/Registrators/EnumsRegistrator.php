<?php
declare(strict_types = 1);

namespace FS\GraphQL\DI\Registrators;

use FS\GraphQL\DI\GraphQLExtension;
use GraphQL\Type\Definition\EnumType;

class EnumsRegistrator
{

	/**
	 * @param GraphQLExtension $extension
	 * @param array $enums
	 */
	public function register(GraphQLExtension $extension, array $enums): void
	{
		$builder = $extension->getContainerBuilder();
		foreach ($enums as $enumName => $enumDetails) {
			$builder
				->addDefinition($extension->prefix("enum.$enumName"))
				->setClass(EnumType::class)
				->setArguments([
					'config' => [
						'name' => $enumName,
						'values' => $this->buildEnumValues($enumDetails),
						'description' => isset($enumDetails['description']) ? $enumDetails['description'] : null,
					],
				]);
		}
	}

	/**
	 * @param array $enumDetails
	 * @return array
	 */
	private function buildEnumValues(array $enumDetails): array
	{
		$output = [];
		foreach ($enumDetails as $enumName => $enumDetail) {
			$output[$enumName] = [
				'value' => $enumDetail,
			];
		}
		return $output;
	}

}
