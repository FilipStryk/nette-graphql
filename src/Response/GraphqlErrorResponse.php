<?php
declare(strict_types = 1);

namespace FS\GraphQL\Response;

use GraphQL\Error\Error;
use GraphQL\Language\SourceLocation;
use GraphQL\Utils;
use Nette\Application\Responses\JsonResponse;
use Nette\Http\IResponse;

class GraphqlErrorResponse extends JsonResponse
{

	/**
	 * @var int
	 */
	private $code;

	/**
	 * GraphqlErrorResponse constructor.
	 * @param $errorMessage
	 * @param int $code
	 * @param bool $withDataField
	 */
	public function __construct($errorMessage, int $code = IResponse::S500_INTERNAL_SERVER_ERROR, $withDataField = TRUE)
	{
		$this->code = $code;
		$payload = new \stdClass;
		if ($withDataField) {
			$payload->data = NULL;
		}

		if(is_string($errorMessage)) {
			$payload->errors = [
				['message' => $errorMessage],
			];
		} else {
			foreach ($errorMessage as $error) {
				if (!$error instanceof Error) {
					$exceptionMessage = 'If you are sending array to the ' . __METHOD__ . ' it has to be array of ' . Error::class;
					throw new \InvalidArgumentException($exceptionMessage);
				}
				$payload->errors[] = [
					'message' => $error->getMessage(),
					'locations' => Utils::map($error->getLocations(), function (SourceLocation $loc) {
						return $loc->toSerializableArray();
					}),
				];
			}
		}

		parent::__construct($payload, NULL);
	}

	/**
	 * @param \Nette\Http\IRequest $httpRequest
	 * @param IResponse $httpResponse
	 */
	public function send(\Nette\Http\IRequest $httpRequest, IResponse $httpResponse)
	{
		$httpResponse->setCode($this->code);
		parent::send($httpRequest, $httpResponse);
	}

	/**
	 * @return int
	 */
	public function getCode(): int
	{
		return $this->code;
	}

}
