<?php
declare(strict_types = 1);

namespace FS\GraphQL\Exception;

class UnknownTypeDefinitionException extends \Exception
{

}
