<?php
declare(strict_types = 1);

namespace FS\GraphQL;

use GraphQL\Schema;
use GraphQL\Type\Definition\ObjectType;

class SchemaFactory
{

	/**
	 * @var ObjectType
	 */
	private $queryType;

	/**
	 * @var ObjectType
	 */
	private $mutationType;

	/**
	 * @param ObjectType $queryType
	 */
	public function addQueryType(ObjectType $queryType)
	{
		$this->queryType = $queryType;
	}

	/**
	 * @param ObjectType $mutationType
	 */
	public function addMutationType(ObjectType $mutationType)
	{
		$this->mutationType = $mutationType;
	}

	/**
	 * @return Schema
	 */
	public function build(): Schema
	{
		return new Schema([
			'query' => $this->queryType,
			'mutation' => $this->mutationType,
		]);
	}

}
