<?php
declare(strict_types = 1);

namespace FS\GraphQL\Types;


use GraphQL\Error\Error;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;

class DateTimeType extends ScalarType
{

	private const JS_ATOM = 'Y-m-d\TH:i:s.uP';

	public $name = 'DateTime';

	public $description = 'The `DateTime` scalar type represents date/time format compatible with ISO 8601 format.';

	/**
	 * @param mixed $value
	 * @return string
	 */
	public function serialize($value): string
	{
		if (!$value instanceof \DateTimeInterface) {
			throw new \UnexpectedValueException("Cannot represent value as {$this->name}: " . \GraphQL\Utils::printSafe($value));
		}
		return $value->format('c');
	}

	/**
	 * @param mixed $value
	 * @return \DateTimeInterface
	 */
	public function parseValue($value): \DateTimeInterface
	{
		//NOTE: DATE_ISO8601 format is not compatible with ISO-8601, but is left this way in PHP for backward compatibility reasons.
		//  http://php.net/manual/en/class.datetime.php#datetime.constants.types
		$dateTime = \DateTimeImmutable::createFromFormat(\DateTime::ATOM, $value);
		if ($dateTime === FALSE) { // compatibility with Javascript
			$dateTime = \DateTimeImmutable::createFromFormat(self::JS_ATOM, $value);
		}
		if ($dateTime === FALSE) {
			throw new \UnexpectedValueException('Not a valid ISO 8601 date format.');
		}
		return $dateTime;
	}

	/**
	 * @param \GraphQL\Language\AST\Node $valueNode
	 * @return \DateTimeInterface
	 * @throws Error
	 */
	public function parseLiteral($valueNode): \DateTimeInterface
	{
		if (!$valueNode instanceof StringValueNode) {
			throw new Error('Query error: Can only parse strings got: ' . $valueNode->kind, [$valueNode]);
		}
		return $this->parseValue($valueNode->value);
	}

}
